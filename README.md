DevTools

1.2.0
Optimized for GNU/Linux distributions.

Igor Nunes, aka thoga31 @ www.portugal-a-programar.pt

Licensed under the GNU GPL 3.0 (https://www.gnu.org/copyleft/gpl.html)

Language: Object Pascal (using Free Pascal Compiler, only)

This application is a collection of small tools that might be useful for programmers in general, specially those who program for a console environment.

It includes an ASCII Table, an ASCII Converter, an ASCII Key Informer and a Color Pallete.