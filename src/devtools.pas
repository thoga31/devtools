(*
====== DEVTOOLS 1.2.0 ======
A set of tools for developers.
Includes:
    - ASCII Table;
    - ASCII Converter;
    - ASCII Key Informer;
    - Console Color Palette (codes for Pascal);

Changelog:
    - Key Informer: better support for UTF-8 (only for Linux).

  By: Igor Nunes, aka thoga31
Date: July, 2013

Licensed under the GNU GPL 3.0 (included with the source)
*)

{$mode objfpc}
program devtools;
uses crt, sysutils, strutils;

type TAsciiType = (AsciiSpecial, AsciiStandard, AsciiExtended);
     TAsciiChar = record
        order : byte;
        hex : string[2];
        character : string[3];
        CharType : TAsciiType;
     end;
     TAsciiTable = array of TAsciiChar;

const ASCII_LENGTH = 255;
      KEY_ESC = #27;
      MAIN_REDIRECT_OPTIONS = [KEY_ESC, '1'..'4'];
      STREMPTY : string = '';
      {$ifdef win32}
      CHAR_LINE_HORIZONTAL = #196;
      CHAR_LINE_VERTICAL   = #179;
      CHAR_CORNER_UPLEFT   = #218;
      CHAR_CORNER_UPRIGHT  = #191;
      {$else}
      CHAR_LINE_HORIZONTAL = '─';
      CHAR_LINE_VERTICAL   = '│';
      CHAR_CORNER_UPLEFT   = '┌';
      CHAR_CORNER_UPRIGHT  = '┐';
      {$endif}

      SPECIAL : array [0..32] of string = ('NUL', 'SOH', 'ETX', 'EOT', 'ENQ', 'ACK', 'BEL', 'BS', 'TAB', 'TAB', 'LF', 'VT', 'FF', 'CR', 'SO', 'SI', 'DLE', 'DC1', 'DC2', 'DC3', 'DC4', 'NAK', 'SYN', 'ETB', 'CAN', 'EM', 'SUB', 'ESC', 'FS', 'GS', 'RS', 'US', 'Spc');
      DEL_ORD = 127;

var table : TAsciiTable;
    option : char;
    INTERACTIVE : boolean;
    ParamIter : word;
    temp_bool : boolean;


procedure Pause;
begin
    repeat
    until readkey = #13;
end;

procedure Negate(var bool : boolean);
begin
    bool := not bool;
end;

function IfThenElse(b : boolean; t, f : byte) : byte;
begin
    if b then IfThenElse := t
         else IfThenElse := f;
end;

function RepeatChar(c : {$ifdef win32}char{$else}string{$endif}; n : byte) : string;
var i : byte;
begin
    RepeatChar := STREMPTY;
    for i:=1 to n do RepeatChar := RepeatChar + c;
end;


function CharType2Str(c : TAsciiType) : string;
begin
    case c of
        AsciiSpecial  : CharType2Str := 'Special';
        AsciiStandard : CharType2Str := 'Standard';
        AsciiExtended : CharType2Str := 'Extended';
    end;
end;


procedure InitializeAsciiTable(var t : TAsciiTable);
var i : byte;
{const SPECIAL : array [0..32] of string = ('NUL', 'SOH', 'ETX', 'EOT', 'ENQ', 'ACK', 'BEL', 'BS', 'TAB', 'TAB', 'LF', 'VT', 'FF', 'CR', 'SO', 'SI', 'DLE', 'DC1', 'DC2', 'DC3', 'DC4', 'NAK', 'SYN', 'ETB', 'CAN', 'EM', 'SUB', 'ESC', 'FS', 'GS', 'RS', 'US', 'Spc');
      DEL_ORD = 127;}
begin
    SetLength(t, ASCII_LENGTH);

    for i := 0 to ASCII_LENGTH do
        with t[i] do begin
            order := i;
            hex := IntToHex(i, 2);
            case i of
                0..32 : begin
                        character := special[i];
                        CharType := AsciiSpecial;
                        end;
                DEL_ORD : begin
                          character := 'DEL';
                          CharType := AsciiSpecial;
                          end;
                33..(DEL_ORD-1) : begin
                                  character := char(i);
                                  CharType := AsciiStandard;
                                  end;
            else begin
                 character := char(i);
                 CharType := AsciiExtended;
                 end;
            end;
        end;
end;


procedure ViewTable(const INTERACT : boolean = true; MODE_EXT : boolean = false);
const COLUMNS = 7;
      KEYS_NAVIGATION = [#75, #77];
var ModeExtended : boolean = false;
    i, l, c, ch : word;
    key : char;
    LINES : byte;

    procedure CharColor(t : TAsciiType);
    begin
       case t of
           AsciiSpecial : textcolor(14);
           AsciiStandard : textcolor(10);
           AsciiExtended : textcolor(11);
       end;
    end;

    function GiveSpace(s : string; min : word) : string;
    var count : word;
    begin
        GiveSpace := s;
        for count := length(s)+1 to min do GiveSpace := GiveSpace + ' ';
    end;

begin
    repeat
        if INTERACT then begin
            clrscr;
            textcolor(11);
            writeln(CHAR_LINE_VERTICAL, '  ASCII Table  ', CHAR_LINE_VERTICAL, IfThen(ModeExtended, '  Extended  ', '  Standard  '), CHAR_LINE_VERTICAL, '  [<-][->] Navigation  ', CHAR_LINE_VERTICAL);
        end;

        LINES := 128 div COLUMNS + IfThenElse(128 mod COLUMNS = 0, 0, 1);
        i := IfThenElse(ModeExtended, 128, 0);
        l := 0;
        repeat
            for c:=0 to COLUMNS do begin
                ch := l + c * LINES + IfThenElse(ModeExtended, 128, 0);
                if (ch > IfThenElse(ModeExtended, 255, 127)) then break;
                with table[ch] do begin
                    textcolor(7); write(order:3, hex:3);
                    CharColor(CharType); write(' ', GiveSpace(character, 4));
                end;
                inc(i);
            end;
            writeln;
            inc(l);
        until (i > IfThenElse(ModeExtended, 255, 127));

        if INTERACT then begin
            repeat
                key := readkey;
            until (key in [KEY_ESC] + KEYS_NAVIGATION);
            if key in KEYS_NAVIGATION then Negate(ModeExtended);
        end else
            if MODE_EXT then begin
                Negate(ModeExtended);
                Negate(MODE_EXT);
            end else key := KEY_ESC;
    until (key = KEY_ESC);
end;


procedure Converter(const INTERACT : boolean = true; opt : char = #13; s : string = '');
const REDIR_OPTIONS = [KEY_ESC, '1', '2', '3'];
begin
    repeat
        if INTERACT then begin
            clrscr;
            textcolor(11);
            writeln(CHAR_LINE_VERTICAL, '  ASCII Converter  ', CHAR_LINE_VERTICAL);
            textcolor(14);
            writeln('The converter doesn''t work with special characters!');
            textcolor(7);
            writeln;
            writeln('   1 >  Ord -> Char & Hex');
            writeln('   2 > Char -> Ord  & Hex');
            writeln('   3 >  Hex -> Char & Ord');
            writeln(' ESC > Back');
            repeat
                opt := readkey;
            until (opt in REDIR_OPTIONS);
            clrscr;
        end;

        case opt of
            '1' : begin
                  if INTERACT then begin
                      textcolor(11);
                      writeln(CHAR_LINE_VERTICAL, '  ASCII Converter  ', CHAR_LINE_VERTICAL , '  Ord -> Char & Hex  ', CHAR_LINE_VERTICAL);
                      textcolor(15); writeln; write('Ord? ');
                      textcolor(7);  readln(s);
                  end;
                  writeln('    Hex = ', table[StrToInt(s)].hex);
                  writeln('   Char = ', table[StrToInt(s)].character);
                  writeln('   Type = ', CharType2Str(table[StrToInt(s)].CharType));
                  if INTERACT then Pause;
                  end;
            '2' : begin
                  if INTERACT then begin
                      textcolor(11);
                      writeln(CHAR_LINE_VERTICAL, '  ASCII Converter  ', CHAR_LINE_VERTICAL , '  Char -> Ord & Hex  ', CHAR_LINE_VERTICAL);
                      textcolor(15); writeln; write('Char? ');
                      textcolor(7);  readln(s);
                  end;
                  writeln('    Hex = ', table[ord(s[1])].hex);
                  writeln('    Ord = ', table[ord(s[1])].order);
                  writeln('   Type = ', CharType2Str(table[ord(s[1])].CharType));
                  if INTERACT then Pause;
                  end;
            '3' : begin
                  if INTERACT then begin
                      textcolor(11);
                      writeln(CHAR_LINE_VERTICAL, '  ASCII Converter  ', CHAR_LINE_VERTICAL , '  Hex -> Char & Ord  ', CHAR_LINE_VERTICAL);
                      textcolor(15); writeln; write('Hex? ');
                      textcolor(7);  readln(s);
                  end;
                  writeln('    Ord = ', table[Hex2Dec(s)].order);
                  writeln('   Char = ', table[Hex2Dec(s)].character);
                  writeln('   Type = ', CharType2Str(table[Hex2Dec(s)].CharType));
                  if INTERACT then Pause;
                  end;
        end;

        if not INTERACT then opt := KEY_ESC;
    until opt = KEY_ESC;
end;


procedure ColorPalette(const INTERACT : boolean = true);
const MAX_TEXTBACKGROUND = 7;
      MAX_TEXTCOLOR = 15;
var x, y : integer;
begin
    if INTERACT then begin
        clrscr;
        textcolor(11);
        writeln(CHAR_LINE_VERTICAL, '  Console Color Palette  ', CHAR_LINE_VERTICAL);
    end;
    for y := -1 to MAX_TEXTCOLOR do begin
        textcolor(IfThenElse(y = -1, 15, y));
        for x := 0 to MAX_TEXTBACKGROUND do begin
            textbackground(IfThenElse(y = -1, 0, x));
            write(IfThenElse(y = -1, x, y):2, IfThen(y = -1, '      ', ' Teste'));
            textbackground(0);
            write(' ');
        end;
        writeln;
    end;
    textbackground(0);
    textcolor(7);
    if INTERACT then Pause;
end;


procedure KInformer(const INTERACT : boolean = true);
CONST KEY_END = 27;
var ch : byte;
    a : string[10];
    bs : array[0..4] of byte;
    n, i : integer;
    c : cardinal;
    fn : boolean;
begin
    if INTERACT then begin
        clrscr;
        textcolor(11);
        writeln(CHAR_LINE_VERTICAL, '  Key Informer  ', CHAR_LINE_VERTICAL);
    end;

    repeat
        fn := false;
        n := 0;
        for i := 0 to 4 do
            bs[i] := 0;
        ch := ord(readkey);
        bs[0] := ch;
        if (ch and $80) = $80 then begin
            //write('>> ');
            {$ifdef linux}
            if (ch shr 3) = $1E then
                n := 3
            else if (ch shr 4) = $0E then
                n := 2
            else if (ch shr 5) = $06 then
                n := 1;
            i := 0;
            repeat
                bs[i + 1] := ord(readkey);
                inc(i);
            until i = n;
            {$endif}
        end else if ch = 0 then begin
            fn := true;
            bs[0] := ord(readkey);
        end;

        setstring(a, @bs[0], i + 1);

        if (not fn) and (n = 0) and ((bs[0] < 33) or (bs[0] = DEL_ORD)) then begin
            if bs[0] < 33 then
                write(SPECIAL[bs[0]]:3, ' (')
            else if bs[0] = del_ord then
                write('DEL (');
        end else if fn then begin
            write('NUL + ''', a, ''' (0x00 + ');
        end else
            write('''', a, ''' (');

        for i := 0 to n do begin
            if (i > 0) then
                write(', ');
            write('0x', inttohex(bs[i], 2), '');
        end;

        write(') ');
        c := 0;

        case n of
            0 : begin
                  if bs[0] > 127 then write('E-');
                  write('ASCII');
                  if fn then write(' (Function)');
                end;
            1 : begin
                  c := ((bs[0] and $1F) shl 6) or (bs[1] and $3F);
                  write('U+', inttohex(c, 4));
                end;
            2 : begin
                  c := ((bs[0] and $0F) shl 12) or ((bs[1] and $3F) shl 6) or (bs[2] and $3F);
                  write('U+', inttohex(c, 4));
                end;
            3 : begin
                  c := ((bs[0] and $07) shl 18) or ((bs[1] and $3F) shl 12) or ((bs[2] and $3F) shl 6) or (bs[3] and $3F);
                  write('U+', inttohex(c, 6));
                end;
        end;
        writeln;
    until (ch = KEY_END);
end;


begin
    try
        INTERACTIVE := (ParamCount = 0) or ((ParamCount = 1) and (Upcase(ParamStr(1)) = '--INTERACTIVE'));
        InitializeAsciiTable(table);

        if not INTERACTIVE then begin
            
            ParamIter := 1;
            repeat
                if (Upcase(ParamStr(ParamIter)) = '--ASCII') or (Upcase(ParamStr(ParamIter)) = '--TABLE') then begin
                    inc(ParamIter);
                    if (ParamCount >= ParamIter) then
                        if (Upcase(ParamStr(ParamIter)) = 'TRUE') or (Upcase(ParamStr(ParamIter)) = 'EXTENDED') then
                            temp_bool := true
                        else if (Upcase(ParamStr(ParamIter)) = 'FALSE') or (Upcase(ParamStr(ParamIter)) = 'STANDARD') then
                            temp_bool := false
                        else
                            raise exception.Create('AT ' + IntToStr(ParamIter) + ': Wrong option definition [ASCII Table, option ''EXTENDED?''].')
                    else temp_bool := false;
                    ViewTable(INTERACTIVE, temp_bool);
                    textbackground(0); textcolor(7);
                    {$ifdef win32} Pause; {$else} halt(0); {$endif}

                end else if Upcase(ParamStr(ParamIter)) = '--COLOR' then begin
                    ColorPalette(INTERACTIVE);
                    textbackground(0); textcolor(7);
                    {$ifdef win32} Pause; {$else} halt(0); {$endif}

                end else if (Upcase(ParamStr(ParamIter)) = '--CONVERTER') or (Upcase(ParamStr(ParamIter)) = '--CONV') then begin
                    inc(ParamIter);
                    if (Upcase(ParamStr(ParamIter)) = 'ASCII') or (Upcase(ParamStr(ParamIter)) = 'CHAR') or (Upcase(ParamStr(ParamIter)) = 'CH') then
                        Converter(INTERACTIVE, '2', ParamStr(ParamIter+1))
                    else if (Upcase(ParamStr(ParamIter)) = 'HEXADECIMAL') or (Upcase(ParamStr(ParamIter)) = 'HEX') or (Upcase(ParamStr(ParamIter)) = 'X') then
                        Converter(INTERACTIVE, '3', ParamStr(ParamIter+1))
                    else if (Upcase(ParamStr(ParamIter)) = 'ORDER') or (Upcase(ParamStr(ParamIter)) = 'ORD') then
                        Converter(INTERACTIVE, '1', ParamStr(ParamIter+1))
                    else
                        raise exception.Create('AT ' + IntToStr(ParamIter) + ': Wrong identifier definition [ASCII Converter, identifier ''INPUT UNIT''].');
                    textbackground(0); textcolor(7);
                    {$ifdef win32} Pause; {$else} halt(0); {$endif}

                end else if (Upcase(ParamStr(ParamIter)) = '--INFORMER') or (Upcase(ParamStr(ParamIter)) = '--KEY') then begin
                    KInformer(INTERACTIVE);
                    {$ifdef win32} Pause; {$else} halt(0); {$endif}

                end else if (Upcase(ParamStr(ParamIter)) = '--HELP') or (ParamStr(ParamIter) = '-?') then begin
                    textcolor(11);
                    writeln('|    DEVTOOLS    | for GNU/Linux | version 1.2.0 |');
                    writeln('Igor Nunes (aka thoga31) | July 2013');
                    textcolor(15);
                    write('Usage: ');
                    textcolor(14);
                    writeln('devtools [<[--interactive] | --table [<standard | extended>] | --conv <char | ord | hex> C | --color | --key>]');
                    textcolor(15);
                    writeln('Modes:');
                    textcolor(7);
                    writeln('There are two modes: interactive and non-interactive. If you don''t specify any argument, the program will start in interactive mode.');
                    textcolor(15);
                    writeln('Non-interactive mode options:');
                    textcolor(10);
                    //writeln('                                                                                ');
                    writeln('--ascii / --table [<options>]    Shows the ASCII table.');
                    textcolor(7);
                    writeln('   Where ''options'' = ''standard/false | extended/true'':');
                    writeln('      ''standard/false''  Shows the standard ASCII table;');
                    writeln('      ''extended/true''   Shows both the standard and the extended ASCII tables.');
                    textcolor(10);
                    writeln('--converter / --conv <input> C   Access the ASCII converter.');
                    textcolor(7);
                    writeln('   Where ''input'' = ascii/char/ch | hexadecimal/hex/x | order/ord');
                    writeln('      ''ascii/char/ch''      Takes an character and returns its Ord and Hex;');
                    writeln('      ''hexadecimal/hex/x''  Takes a hexadecimal and returns its Char and Ord;');
                    writeln('      ''order/ord''          Takes an order and returns its Char and Hex.');
                    textcolor(10);
                    writeln('--color                          Shows the console color palette.');
                    textcolor(10);
                    writeln('--key / --informer               Starts the interactive key informer.');
                    writeln('--help / -?                      Shows this help.');
                    textbackground(0); textcolor(7);
                    {$ifdef win32} Pause; {$else} halt(0); {$endif}

                end;

                inc(ParamIter);
            until (ParamIter > ParamCount);

        end else begin

            repeat
                clrscr;
                textcolor(11);
                writeln(CHAR_LINE_VERTICAL, '  DEVTOOLS 1.2.0  ', CHAR_LINE_VERTICAL, '  Igor Nunes, aka thoga31  ', CHAR_LINE_VERTICAL, '  July 2013  ', CHAR_LINE_VERTICAL);
                textcolor(7);
                writeln;
                writeln('   1 > ASCII Table');
                writeln('   2 > ASCII Converter');
                writeln('   3 > Key Informer');
                writeln('   4 > Console Color Palette');
                writeln(' ESC > Exit');

                repeat
                    option := readkey;
                until (option in MAIN_REDIRECT_OPTIONS);

                case option of
                    '1' : ViewTable;
                    '2' : Converter;
                    '3' : KInformer;
                    '4' : ColorPalette;
                end;
            until (option = KEY_ESC);

        end;

    except
        on e:exception do begin
            if INTERACTIVE then clrscr;
            textbackground(0);
            writeln;
            textcolor(12);
            writeln(Upcase(e.ClassName), ': ', e.message);
            textbackground(0); textcolor(7);
            if INTERACTIVE then Pause
                           else {$ifdef win32} Pause; {$else} halt(1); {$endif}
        end;
    end;
end.
